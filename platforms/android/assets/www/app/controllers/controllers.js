var appControllers = angular.module('appControllers', []);

appControllers.controller('HomeCtrl', ['$scope', '$location', 'checkCreds', function($scope, $location, checkCreds) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
}]);

appControllers.controller('HistoricoCtrl', ['$scope', '$location', 'checkCreds', function($scope, $location, checkCreds) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
}]);

appControllers.controller('HorarioCtrl', ['$scope', '$location', 'checkCreds', 'getId', function($scope, $location, checkCreds, getId) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
    var id = getId();
    $scope.status = "editar";

    $scope.editSave = function(){
        if ($scope.status === "editar") {
            console.log("DOM "+$scope.domingo);
            $scope.status = "salvar";
        } else {//salvar
            console.log("SEG "+$scope.segunda);
        }
    };
}]);

appControllers.controller('LoginCtrl', ['$scope', '$location', 'Login', 'setCreds', 'checkCreds', '$http',
    function LoginCtrl($scope, $location, Login, setCreds, checkCreds, $http) {
        if (checkCreds()) {
            $scope.activetab = $location.path('/');
        }

        $scope.submit = function() {
            $scope.sub = true;
            var postData = {
                "username": $scope.username,
                "password": $scope.password
            };

            Login.login({}, postData,
                function success(response) {
                    console.log("Success:" + JSON.stringify(response));
                    $scope.error = "";
                    if (response.authenticated) {
                        setCreds($scope.username, $scope.password,response.perfil._id)
                        $scope.activetab = $location.path('/');
                    } else {
                        console.log("Login Failed");
                        $scope.error = "usuário/senha inválidos!"
                    }

                },
                function error(errorResponse) {
                    console.log("Error:" + JSON.stringify(errorResponse));
                }
            );
        };
    }
]);

appControllers.controller('LogoutCtrl', ['$scope', '$location', 'deleteCreds',
    function LogoutCtrl($scope, $location, deleteCreds) {
        deleteCreds();
        $scope.activetab = $location.path('/login');
    }
]);
