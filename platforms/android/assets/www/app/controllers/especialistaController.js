appControllers.controller('EspecialistaCtrl', ['$scope', '$location', 'Especialista', 'Profissao', 'setCreds', function($scope, $location, Especialista, Profissao, setCreds) {

    Profissao.getProfissoes.listar({},
            function success(response) {
               console.log("Success:" + JSON.stringify(response));
               $scope.profissao = response;
               $scope.profissao_selected = $scope.profissao[0];
            },
            function error(errorResponse) {
               console.log("Error:" + JSON.stringify(errorResponse));
            }
    );

    $scope.submit = function() {
        var postData = {
            "nome": $scope.nome,
            "telefone": $scope.telefone,
            "cpf": $scope.cpf,
            "email": $scope.email,
            "usuario": $scope.usuario,
            "senha": $scope.senha,
            "endereco": {
                "logradouro": $scope.logradouro,
                "numero": $scope.numero,
                "bairro": $scope.bairro,
                "cidade": $scope.cidade,
                "cep": $scope.cep
            },
            "profissoes_id": $scope.profissao_selected._id,
            "experiencias" : $scope.experiencias
        };
        Especialista.salvar.salvar({}, postData,
            function success(response) {
                console.log("Success:" + JSON.stringify(response));
                setCreds($scope.usuario, $scope.senha)
                $scope.activetab = $location.path('/');
            },
            function error(errorResponse) {
                console.log("Error:" + JSON.stringify(errorResponse));
            }
        );
    }
}]);
