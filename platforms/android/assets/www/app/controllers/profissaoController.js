appControllers.controller('SolicitacaoCtrl', ['$scope', '$location', '$http', 'Profissao', 'checkCreds', 'getToken', function($scope, $location, $http, Profissao, checkCreds, getToken) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
    //$http.defaults.headers.common['Authorization'] = 'Basic ' + getToken();

    Profissao.listar({},
        function success(response) {
            console.log("Success:" + JSON.stringify(response));
            $scope.profissao = response;
        },
        function error(errorResponse) {
            console.log("Error:" + JSON.stringify(errorResponse));
        }
    );
    $scope.selected = [];
}]);
