var appServicesApi = angular.module('appServicesApi', ['ngResource']);

appServicesApi.factory('Especialista', ['$resource',
    function($resource) {
        return {
            salvar: $resource('http://agendae.mfs.eng.br/api/especialista', {}, {
                salvar: { method: 'POST', cache: false, isArray: false }
            }),
            getEspecialista: $resource('http://agendae.mfs.eng.br/api/especialista/id/:idespecialista', {}, {
                go: { method: 'GET', cache: false, isArray: false },
                edit: { method: 'POST', cache: false, isArray: false }
            })
        }
    }
]);

appServicesApi.factory('Login', ['$resource',
    function($resource) {
        return $resource('http://agendae.mfs.eng.br/api/especialista/login', {}, {
            login: { method: 'POST', cache: false, isArray: false }
        });
    }
]);

appServicesApi.factory('Profissao', ['$resource',
    function($resource) {
        return {
            getProfissoes: $resource('http://agendae.mfs.eng.br/api/profissao', {}, {
                listar: { method: 'GET', cache: false, isArray: true }
            }),
            getProfissao: $resource('http://agendae.mfs.eng.br/api/profissao/:idProfissao', {}, {
                listar: { method: 'GET', cache: false, isArray: false }
            })
        }
    }
]);
