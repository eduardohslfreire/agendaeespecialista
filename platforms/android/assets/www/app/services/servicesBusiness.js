var appServicesBusiness = angular.module('appServicesBusiness', []);

appServicesBusiness.factory('checkCreds', ['$cookies', function($cookies) {
        return function() {
            var returnVal = false;
            var appCreds = $cookies.appCreds;
            if (appCreds !== undefined && appCreds !== "") {
                returnVal = true;
            }
            return returnVal;
        };

    }]);

appServicesBusiness.factory('getToken', ['$cookies', function($cookies) {
        return function() {
            var returnVal = "";
            var appCreds = $cookies.appCreds;
            if (appCreds !== undefined && appCreds !== "") {
                returnVal = btoa(appCreds);
            }
            return returnVal;
        };

    }]);

appServicesBusiness.factory('getUsername', ['$cookies', function($cookies) {
        return function() {
            var returnVal = "";
            var appUsername = $cookies.appUsername;
            if (appUsername !== undefined && appUsername !== "") {
                returnVal = appUsername;
            }
            return returnVal;
        };

    }]);

    appServicesBusiness.factory('getId', ['$cookies', function($cookies) {
        return function() {
            var returnVal = "";
            var idespecialista = $cookies.id;
            if (idespecialista !== undefined && idespecialista !== "") {
                returnVal = idespecialista;
            }
            return returnVal;
        };

    }]);

appServicesBusiness.factory('setCreds', ['$cookies', function($cookies) {
        return function(un, pw,idespecialista) {
            var token = un.concat(":", pw);
            $cookies.appCreds = token;
            $cookies.appUsername = un;
            $cookies.id = idespecialista;
        };

    }]);

appServicesBusiness.factory('deleteCreds', ['$cookies', function($cookies) {
        return function() {
            $cookies.appCreds = "";
            $cookies.appUsername = "";
            $cookies.id = "";
        };
    }]);
