appControllers.controller('Perfil2Ctrl', ['$scope', '$location', 'checkCreds', function ($scope, $location, checkCreds) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
    $scope.activetab = $location.path('/perfil');
}]);

appControllers.controller('PerfilCtrl', ['Especialista', 'Profissao', '$scope', '$rootScope','$cookies','$location', 'checkCreds', 'getId',function(Especialista,Profissao,$scope,$rootScope,$cookies, $location, checkCreds, getId) {
    if (!checkCreds()) {
        $scope.activetab = $location.path('/login');
    }
    var id = getId();
    $scope.status = "editar";

    Especialista.getEspecialista.go({ idespecialista: id },
        function success(response) {
            //console.log(JSON.stringify(response));
            $scope.nome = response.nome;
            $scope.telefone = response.telefone;
            $scope.cpf = response.cpf;
            $scope.email = response.email;
            $scope.usuario = response.usuario;
            $scope.senha = response.senha;
            $scope.logradouro = response.endereco.logradouro;
            $scope.numero = response.endereco.numero;
            $scope.bairro = response.endereco.bairro;
            $scope.cidade = response.endereco.cidade;
            $scope.cep = response.endereco.cep;
            $scope.experiencias = response.experiencias;
            //scope.profissao_selected = response.profissoes_id;
            Profissao.getProfissao.listar({idProfissao: response.profissoes_id},
                function success(response) {
                   console.log("Success:" + JSON.stringify(response));
                   $scope.profissao_selected = response;
                   //$scope.profissao_selected = $scope.profissao[0];
                },
                function error(errorResponse) {
                   console.log("Error:" + JSON.stringify(errorResponse));
                }
            );
                                  
        },
        function error(errorResponse) {
            console.log("Error:" + JSON.stringify(errorResponse));
        });

    Profissao.getProfissoes.listar({},
        function success(response) {
           console.log("Success:" + JSON.stringify(response));
           $scope.profissao = response;
           //$scope.profissao_selected = $scope.profissao[0];
        },
        function error(errorResponse) {
           console.log("Error:" + JSON.stringify(errorResponse));
        }
    );

    $scope.cancelar = function () {
        console.log("cancelar");
        $scope.activetab = $location.path('/home');
    }

    $scope.sair = function () {
        console.log("sair");
        $scope.activetab = $location.path('/logout');
    }


    $scope.editSave = function () {
        if ($scope.status === "editar") {
            //console.log("Modo Edicao");
            $scope.status = "salvar";
        } else {//salvar
            var postData = {
                "nome": $scope.nome,
                "telefone": $scope.telefone,
                "cpf": $scope.cpf,
                "email": $scope.email,
                "usuario": $scope.usuario,
                "senha": $scope.senha,
                "endereco": {
                    "logradouro": $scope.logradouro,
                    "numero": $scope.numero,
                    "bairro": $scope.bairro,
                    "cidade": $scope.cidade,
                    "cep": $scope.cep
                },
                "profissoes_id": $scope.profissao_selected._id,
                "experiencias" : $scope.experiencias
            };
            Especialista.getEspecialista.edit({ idespecialista: id },postData,
                function success(response) {
                },
                function error(errorResponse) {
                    console.log("Error:" + JSON.stringify(errorResponse));
                }
            );
            $scope.status = "editar";
        }
    }

    /*
    $scope.goDefinirHorario = function() {
        console.log($cookies);
        Especialista.getEspecialista.go({ idespecialista: $cookies.id },
            function success(response) {
                console.log(JSON.stringify(response));
                $rootScope.horario_agenda=response.horario_agenda;
                $scope.activetab = $location.path('/perfil/horario');                
            },
            function error(errorResponse) {
                console.log("Error:" + JSON.stringify(errorResponse));
            });
        
    };*/
}]);