var appDirectives = angular.module('appDirectives', []);

appDirectives.directive('appMenu', function() {
    return {
        restrict: 'A',
        templateUrl: 'app/views/partials/menu.html',
        link: function(scope, el, attrs) {
            scope.label = attrs.menuTitle;
        }
    };
});

appDirectives.directive('appFooter', function() {
    return {
        restrict: 'A',
        templateUrl: 'app/views/partials/footer.html',
        link: function(scope, el, attrs) {
            scope.label = attrs.footerTitle;
        }
    };
});