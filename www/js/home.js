$(document).ready(function() {

    $('#calendar').fullCalendar({
        /*customButtons: {
            HoraUtil: {
                text: 'Solicitações não aprovadas',
                click: function() {
                    alert('clicked the custom button!');
                }
            }
        },
        footer: {
            left: 'month,agendaWeek,agendaDay',
            right: 'HoraUtil'
        },*/
        defaultDate: '2017-12-15',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        events: [           
            {
                title: 'Amanda Almeida',
                start: '2017-12-12T05:30:00',
                end: '2017-12-12T05:30:00'
            },            
            {
                title: 'Joao Paulo',
                start: '2017-12-12T06:30:00',
                end: '2017-12-12T06:30:00'
            },
            {
                title: 'Paulo Jose',
                start: '2017-12-12T07:30:00',
                end: '2017-12-12T07:30:00'
            },
            {
                title: 'Matheus Mota',
                start: '2017-12-08T10:30:00',
                end: '2017-12-08T12:30:00'
            },
            {
                title: 'Ricardo Lima',
                start: '2017-12-09T10:30:00',
                end: '2017-12-09T12:30:00'
            },
            {
                title: 'Julia Oliveira',
                start: '2017-12-11T10:30:00',
                end: '2017-12-11T12:30:00'
            },
            {
                title: 'Fabio Costa',
                start: '2017-12-12T09:30:00',
                end: '2017-12-12T10:00:00'
            },            
            {
                title: 'Amador Davi',
                start: '2017-12-12T10:30:00',
                end: '2017-12-12T12:30:00'
            }         
        ]
    });
    $(".fc-prev-button").addClass('fc-state-disabled');
});